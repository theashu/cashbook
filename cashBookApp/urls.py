"""cashBookApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from cashBookApp import views as loginview
from firstapp import views as fappview
from django.conf.urls import url, include

firstapp_manage_urlpatterns = [
    url(r'^insert_or_show/', fappview.cashBook, name='cashbook'),
    url(r'^show_customers/', fappview.showCustomers, name='show_customers'),
    url(r'^add_customers/', fappview.addCustomers, name='add_customers'),
    url(r'^customers/', fappview.customersCashBook, name='customers_cashbook'),
]

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', loginview.userLogin, name='login'),
    url(r'^cashbook/', include(firstapp_manage_urlpatterns)),
]
