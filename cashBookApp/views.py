from django.contrib.auth import authenticate, login
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse, HttpResponseRedirect, Http404

@csrf_exempt
def userLogin(request):
    try:
        if request.method == 'GET':
            return render(request, 'login.html')
        if request.method == 'POST':    
            print "userLogin"
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/cashbook/insert_or_show/')
            else:
                return render(request, 'login.html')
    except Exception as e:
        return e