# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Customers(models.Model):
    name = models.TextField(null=True)
    address = models.TextField(null=True)
    gstIn = models.TextField(null=True)
    phone = models.TextField(null=True)
    updatedAt = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return self.name

class CashbookRow(models.Model):
    customer = models.ForeignKey(Customers,null=True)
    debitedAmount = models.TextField(null=True)
    creditedAmount = models.TextField(null=True)
    updatedAt = models.DateTimeField(auto_now=True, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return self.debitedAmount