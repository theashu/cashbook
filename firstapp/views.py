from __future__ import unicode_literals

import json

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from firstapp.models import *
from django.contrib.auth.decorators import login_required

@login_required
@csrf_exempt
def cashBook(request):
    try:
        if request.method == 'GET':
            customers = Customers.objects.all()
            cashbook = CashbookRow.objects.all()
            return render(request, 'cashbook.html',{'customers':customers,'cashbook':cashbook})
        elif request.method == "POST":
            name = request.POST.getlist('name[]')
            dr = request.POST.getlist('dr[]')
            cr = request.POST.getlist('cr[]')
            for i in range(0,len(name)):
                customer = Customers.objects.get(id=name[i])
                cashBook_obj = CashbookRow(customer=customer,debitedAmount=dr[i],creditedAmount=cr[i])
                cashBook_obj.save()
                print 'saved'
            customers = Customers.objects.all()
            cashbook = CashbookRow.objects.all()
            return render(request, 'cashbook.html', {'customers': customers,'cashbook':cashbook})
    except Exception as e:
        return HttpResponse(e)

@login_required
@csrf_exempt
def customersCashBook(request):
    try:
        if request.method == "POST":
            id = request.POST.get('id')
            customers = Customers.objects.all()
            cashbook = CashbookRow.objects.filter(customer=id)
            return render(request, 'cashbook.html', {'customers': customers, 'cashbook': cashbook})
    except Exception as e:
        return HttpResponse(e)

@login_required
@csrf_exempt
def showCustomers(request):
    try:
        customers = Customers.objects.all()
        return render(request, 'showCustomers.html',{'customers':customers})
    except Exception as e:
        return HttpResponse(e)

@login_required
@csrf_exempt
def addCustomers(request):
    try:
        if request.method == 'GET':
            return render(request, 'addCustomers.html')
        elif request.method == "POST":
            name = request.POST.get('name')
            address = request.POST.get('address')
            gstIn = request.POST.get('gstIn')
            phone = request.POST.get('phone')
            customersObject = Customers(name=name,address=address,gstIn=gstIn,phone=phone)
            customersObject.save()
            return render(request, 'cashbook.html')
    except Exception as e:
        return HttpResponse(e)